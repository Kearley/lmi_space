#!/usr/bin/env python3

''' 
Title: 

Description: 
Blah, blah, blah

Author: dkearley@integrationpartners.com
'''

#################
# Imports 
#################
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


#################
# Main 
#################
url = "https://space.lab.kearley.us/api/juniper/sd/service-management/services/458754"

payload={}
headers = {
  'Authorization': 'Basic c3VwZXI6UmFtNm9uZTkh'
}

response = requests.request("GET", url, headers=headers, data=payload, verify=False)

print(response.text)
