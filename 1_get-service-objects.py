#!/usr/bin/env python3

''' 
Title: 

Description: 
Blah, blah, blah

Author: dkearley@integrationpartners.com
'''

#################
# Imports 
#################
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import os

#################
# Globals 
#################
host = "space.lab.kearley.us"
url = "https://"+ host + "/api/juniper/sd/service-management/services"

payload={}
headers = {
  # 'Accept': 'application/vnd.juniper.sd.service-management.services+json;version=1;q=0.01',
  'Accept': 'application/vnd.juniper.sd.service-management.services+xml;version=1;q=0.01',
  'Authorization': 'Basic c3VwZXI6UmFtNm9uZTkh'
}

mydir = "working-data/responses/"
fname = "services-response.xml"

#################
# Functions 
#################

def main():

  response = requests.request("GET", url, headers=headers, data=payload, verify=False)
  # print(response.text)

  # Create the final directory if it doesn't exist
  if not os.path.exists(mydir):
    os.makedirs(mydir)

  with open(mydir + fname, "w") as f:
      f.write(response.text)

#################
# Main 
#################

if __name__ == "__main__":
    main()