#!/usr/bin/env python3

''' 
Title: 

Description: 
Blah, blah, blah

Author: dkearley@integrationpartners.com
'''

#################
# Imports 
#################
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import os

#################
# Globals 
#################


host = "space.lab.kearley.us"
url = "https://"+ host + "/api/juniper/sd/policy-management/firewall/policies"

payload={}
headers = {
  # 'Accept': 'application/vnd.juniper.sd.policy-management.firewall.policy+json;version=2;q=0.02',
  'Accept': 'application/vnd.juniper.sd.policy-management.firewall.policies+xml;version=2;q=0.02',
  'Authorization': 'Basic c3VwZXI6UmFtNm9uZTkh'
}

mydir = "working-data/responses/"
fname = "policies-response.xml"

#################
# Functions 
#################

def main():
	response = requests.request("GET", url, headers=headers, data=payload, verify=False)
	# print(response.text)

  # Create the final directory if it doesn't exist
	if not os.path.exists(mydir):
		os.makedirs(mydir)
	
	with open(mydir + fname, "w") as f:
		f.write(response.text)

#################
# Main 
#################

if __name__ == "__main__":
    main()