'''
Junos Get Config
Fetches xml configurations from as many Junos devices as you pass to it.
Saves the file as the hostname in the config (if present) or as the IP address you pass it.

Author: dkearley@integrationpartners.com

Dependencies:
- Junos PyEZ (junos-exnc)
- progress
- pprint
- getpass
- argparse

Example:

dkearley@Dereks-MacBook-Pro% python3 get_config.py -d 11.232.104.10 -u ipcadmin
Password:
****************************************************************************************************
Contacting 11.232.104.10... Done!

Running execution time @: 3.661207914352417 seconds


Total execution time: 3.6612319946289062 seconds

Call it 3.661 seconds...

'''


#################
# Imports 
#################
from lxml import etree
from jnpr.junos import Device
import jnpr.junos.exception
import getpass, argparse
import time
import os

#################
# Globals 
#################
line = '*' * 100
config_dir = 'working-data/configs/'

#################
# Functions 
#################
def get_args():
    parser = argparse.ArgumentParser(description="Clean up unused route policies from Junos device or devices.")
    parser.add_argument('-d', '--device', nargs='+', help='The hostname or ip address of the device or \
        list of devices (separated by a space) that you wish to cleanup.')
    parser.add_argument('-u', '--user', help='Junos admin username to use for these device connections.  \
        Defaults to current logged in user.',
        default=getpass.getuser())
    args = parser.parse_args()
    return(args)

def get_config(device, user, passwd):
    # Open a device connection 
    try:
        with Device(host=device, user=user, password=passwd) as dev:
            config = dev.rpc.get_config()
            hostname = dev.facts['hostname']
            return(config,hostname)
    except LockError:
        print('\nError: Configuration was locked!')
    except ConnectRefusedError:
        print('\nError: Device connection refused!')
    except ConnectTimeoutError:
        print('\nError: Device connection timed out!')
    except ConnectAuthError:
        print('\nError: Authentication failure!')
    except ConfigLoadError as err:
        print('\nError: ' + str(err))

#################
# Main 
#################
if __name__ == "__main__":
    # Collect user input and parse for device connections
    args = get_args()
    passwd = getpass.getpass()

    # Start the clock for fun
    test_start = time.time()

    # Create the final directory if it doesn't exist
    if not os.path.exists(config_dir):
        os.makedirs(config_dir)

    # Loop through the devices given and look for items to cleanup
    for device in args.device:
        print(line)
        print("Contacting {}...".format(device), end=" ")
        config, hostname = get_config(device, args.user, passwd)
        try:
            f = open(config_dir + hostname + '.xml', 'w')
        except:
            f = open(config_dir + device + '.xml', 'w')
        f.write(etree.tostring(config, encoding='unicode', pretty_print=True))
        f.close()
        print("Done!")
        print('\nRunning execution time @:', time.time()-test_start,'seconds\n')


    print('\n\nTotal execution time:', time.time()-test_start,'seconds\n')
    print('Call it', round(time.time()-test_start,3),'seconds...\n')