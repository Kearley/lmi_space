#!/usr/bin/env python3

''' 
Title: 

Description: 
Blah, blah, blah

Author: dkearley@integrationpartners.com
'''

#################
# Imports 
#################
from lxml import etree
from tqdm import tqdm

#################
# Globals 
#################
mydir = "working-data/responses/"
infile = "services-response.xml"
outfile = "service-ids.txt"

#################
# Functions 
#################

def main():
    doc = etree.parse(mydir + infile).getroot()
    service_uri_list = doc.xpath('.//service/@uri')
    # print(service_uri_list)
    # for elem in service_uri_list:
    #     print(elem)

    with open(mydir + outfile, "w") as f:
        for elem in tqdm(service_uri_list):
            sid = elem.split('/')[-1]
            f.write("%s\n" % sid)

#################
# Main 
#################

if __name__ == "__main__":
    main()