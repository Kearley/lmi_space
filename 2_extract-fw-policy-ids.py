#!/usr/bin/env python3

''' 
Title: 

Description: 
Blah, blah, blah

Author: dkearley@integrationpartners.com
'''

#################
# Imports 
#################
from lxml import etree
from tqdm import tqdm

#################
# Globals 
#################
mydir = "working-data/responses/"
infile = "policies-response.xml"
outfile = "policy-ids.txt"

#################
# Functions 
#################

def main():
    doc = etree.parse(mydir + infile).getroot()
    policy_uri_list = doc.xpath('.//policy/@uri')
    # print(policy_uri_list)
    # for elem in policy_uri_list:
    #     print(elem)

    with open(mydir + outfile, "w") as f:
        for elem in tqdm(policy_uri_list):
            pid = elem.split('/')[-1]
            f.write("%s\n" % pid)

#################
# Main 
#################

if __name__ == "__main__":
    main()