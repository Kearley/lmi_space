#!/usr/bin/env python3

''' 
Title: 

Description: 
Blah, blah, blah

Author: dkearley@integrationpartners.com
'''

#################
# Imports 
#################
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import os
from lxml import etree
from tqdm import tqdm
import pprint 
import json
import xmltodict

#################
# Globals 
#################
host = "https://space.lab.kearley.us"
space_services_uri = "/api/juniper/sd/service-management/services"
payload={}
headers = {
  'Authorization': 'Basic c3VwZXI6UmFtNm9uZTkh'
}

mydir = "working-data/responses/"
service_response = "services-response.xml"
mydir = "working-data/responses/"
infile = "services-response.xml"
outfile = "service-ids.txt"

#################
# Functions 
#################

def space_get(url):
  response = requests.request("GET", host+url, headers=headers, data=payload, verify=False)
  # print(response.text)
  return (response)


def main():
  # Get all services objects from Space
  space_service_objects_xml = space_get(space_services_uri)

  # Debug statement
  # print(space_service_objects_xml.text)
  
  # Load the XML content and parse out the uri attribute for each service object
  doc = etree.fromstring(space_service_objects_xml.content)
  list_of_service_object_ids = doc.xpath('.//service/@uri')

  # Debug statement
  # print(list_of_service_object_ids)

  # For each service object ID, construct it's URI and make a Space API call (GET) content for that service object.
  # Save all service objects to a list
  service_objects_list = []
  print("\nFetching the objects from Space, please wait...\n")
  for uri in tqdm(list_of_service_object_ids):
    object_url = uri
    service_object = space_get(object_url)

    # Append as xml string
    service_objects_list.append(service_object.text)


  # Debug statements
  pprint.pprint(service_objects_list)
  print(len(service_objects_list))



#################
# Main 
#################

if __name__ == "__main__":
    main()