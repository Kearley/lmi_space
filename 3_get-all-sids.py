#!/usr/bin/env python3

''' 
Title: 

Description: 
Blah, blah, blah

Author: dkearley@integrationpartners.com
'''

#################
# Imports 
#################
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import xmltodict
import json
from tqdm import tqdm
from lxml import etree
import os

#################
# Globals 
#################
host = "space.lab.kearley.us"
service_api = "https://" + host + "/api/juniper/sd/service-management/services/"
infile = "working-data/responses/service-ids.txt"
xmldir = "working-data/service-objects/xml/"
jsondir = "working-data/service-objects/json/"

#################
# Functions 
#################

def write_2_xml(data, sid, mydir):
  # Write out service object to xml file
  if not os.path.exists(mydir):
    os.makedirs(mydir)
  with open(mydir + sid.strip() + ".xml", "w") as f:
    f.write(data)

def write_2_json(data, sid, mydir):
  # Write out service object to json file
  if not os.path.exists(mydir):
    os.makedirs(mydir)
  mydict = xmltodict.parse(data)
  with open(mydir + sid.strip() + ".json", "w") as f:
    json.dump(mydict, f, indent=2)


def main():
  # Open the service_ids file and start reading
  sid_file = open(infile, 'r')
  lines = sid_file.readlines()

  # Iterate over each ID in the file and poke Space for the object
  for sid in tqdm(lines):
    url = service_api + sid.strip()
    # print(url)

    payload={}
    headers = {
      'Authorization': 'Basic c3VwZXI6UmFtNm9uZTkh'
    }

    response = requests.request("GET", url, headers=headers, data=payload, verify=False)
    # print(response.text)

    # # Uncomment to write the xml return to xml file
    write_2_xml(response.text, sid, xmldir)

    # # Uncomment to write the xml return to json file
    write_2_json(response.content, sid, jsondir)


#################
# Main 
#################

if __name__ == "__main__":
    main()