#!/usr/bin/env python3

'''  
Title: 

Description: 
Blah, blah, blah

Author: dkearley@integrationpartners.com
'''

#################
# Imports 
#################
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import xmltodict
import json
from tqdm import tqdm
import os

#################
# Globals 
#################
host = "space.lab.kearley.us"
service_api = "https://" + host + "/api/juniper/sd/policy-management/firewall/policies/"
infile = "working-data/responses/policy-ids.txt"
xmldir = "working-data/policies/xml/"
jsondir = "working-data/policies/json/"

#################
# Functions 
#################

def write_2_xml(xmldata, pid, mydir):
  # Write out service object to xml file
  if not os.path.exists(mydir):
    os.makedirs(mydir)
  with open(mydir + pid.strip() + ".xml", "w") as f:
    f.write(xmldata.text)

def write_2_json(xmldata, pid, mydir):
  # Write out service object to json file
  if not os.path.exists(mydir):
    os.makedirs(mydir)
  mydict = xmltodict.parse(xmldata)
  with open(mydir + pid.strip() + ".json", "w") as f:
    json.dump(mydict, f, indent=2)


def main():
  # Open the service_ids file and start reading
  pid_file = open(infile, 'r')
  lines = pid_file.readlines()

  # Iterate over each ID in the file and poke Space for the object
  for pid in tqdm(lines):
    url = service_api + pid.strip()
    # print(url)

    payload={}
    headers = {
      'Authorization': 'Basic c3VwZXI6UmFtNm9uZTkh'
    }

    response = requests.request("GET", url, headers=headers, data=payload, verify=False)
    # print(response.text)

    # Uncomment to write the xml return to xml file
    write_2_xml(response, pid, xmldir)

    # Uncomment to write the xml return to json file
    write_2_json(response.text, pid, jsondir)


#################
# Main 
#################

if __name__ == "__main__":
    main()