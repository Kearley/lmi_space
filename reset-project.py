#!/usr/bin/env python3

''' 
Title: 

Description: 
Blah, blah, blah

Author: dkearley@integrationpartners.com
'''

#################
# Imports 
#################
import os
import shutil


#################
# Globals 
#################
directories = [ "working-data/configs/", "working-data/policies/", "working-data/responses/", "working-data/service-objects/" ]

#################
# Functions 
#################

def main():
	for f in directories:
		try:
			shutil.rmtree(f)
		except: continue


#################
# Main 
#################
if __name__ == "__main__":
	main()