# Junos Configuration Cleanup Tool
# Program finds unused items in an XML configuration.
# The goal is to identify items that can be deletes or deactivated
#
# Author: dkearley@integrationpartners.com

#################
# Imports 
#################
import os
from lxml import etree
from pprint import pprint as pp
import time

#################
# Globals 
#################

# XML config file name
config_dir = 'working-data/configs/'
conf_files = os.listdir(config_dir)
cleanup_dir = 'cleanup/'
line = '*' * 100


# xpaths
application_groups = "//applications/application/name"
application_objects = "//applications/application-set/name"
address_groups = "//address-book/address-set"
address_objects = "//address-book/address"
applied_addresses = "//policy/policy/match/*[self::source-address or self::destination-address]"
applied_applications = "//policy/policy/match/*[self::application]"


'''
Takes an parsed XML element tree(config) and 2 xpath string statements (string1, string2).
Returns 3 sets/arrays for; all elements from xpath==string1 (total_set), all elements from 
xpath==string2 (assigned_set) and the difference between the two (unassigned_set). Elements 
from xpath==string1 should be a superset of elements from xpath==string2.  
Use: You need to know what defined configuration elements exist but are not applied.
Example: Finding unused accesslists/filters in your config.
'''
def find_unused(config,string1,string2):
    #var sets
    total_set = set()
    assigned_set = set()
    unassigned_set = set()

    # create a list of all elements in the first xpath. Save the text values into a set.
    total_list = config.xpath(string1)
    for elem in total_list:
        total_set.add(elem.text)

    # create a list of all elements in the second xpath. Save the text values into a set.
    assigned_list = config.xpath(string2) 
    for elem in assigned_list:
        assigned_set.add(elem.text)

    # The difference between the two sets are the unassigned elements of the first.
    unassigned_set = total_set - assigned_set

    # return the 3 sets/arrays
    return(total_set, assigned_set, unassigned_set)

'''
Same as find_unused() but takes an additional xpath string statement (string3).  The two
xpath statements get concatenated so you can verify element
application in 2 different xpath locations at once. 
Example: Finding unused route policies in your config (checking both "export" and "import" policies)
'''
def find_unused2(config,string1,string2,string3):
    #var sets
    total_set = set()
    assigned_set = set()
    unassigned_set = set()

    # create a list of all elements in the first xpath. Save the text values into a set.
    total_list = config.xpath(string1)
    for elem in total_list:
        total_set.add(elem.text)

    # create a list of all elements in the second and third xpath. Save the text values into a set.
    assigned_list = config.xpath(string2) + config.xpath(string3)
    for elem in assigned_list:
        assigned_set.add(elem.text)

    # The difference between the two sets are the unassigned elements of the first.
    unassigned_set = total_set - assigned_set

    # return the 3 sets/arrays
    return(total_set, assigned_set, unassigned_set)
    
'''
Takes a list (items) and 2 strings; a cli command statement prefix (string) and a 
filename to write to (filename).  
Use; You have a list of elements found with find_used(), construct CLI commands with them,
and write them to a file that can be used by the operator.
Example: You want to delete any unused acls/filters.
'''
def delete_cleanup(items,string,filename):
    # if items list is not empty, proceed
    if len(items) != 0:
        # open the file to write to
        rem_file = open(filename, "w")
        # iterate over items in the items list, construct the CLI command and write to file
        for item in items:
            rem_file.write("delete " + string + item + "\n")
        # close the file
        rem_file.close()

'''
Takes 3 sets derived from find_unused() and formats to a summary report for stdout.
'''
def report_unused(total,applied,unused):
    print("\tTotal: %s, Unused: %s" % (len(total),len(unused)))
    if len(unused) != 0:
        print("\tUnused:")
        for i in unused:
            print("\t - " + i)
    if len(applied) != 0:
        print("\tApplied:")
        for i in applied:
            print("\t - " + i)


#################
# Main 
#################
if __name__ == "__main__":
    # Start the clock for fun
    test_start = time.time()
    # Loop through the device files and look for items to cleanup
    for file in conf_files: 

        # Parse the XML config for XPath processing
        config = etree.parse(config_dir + file).getroot()

        hostname = config.findtext("system/host-name")

        # Write to remedy files
        print("\n")
        print(line)
        print(hostname)
        print(line)


        # Evaluate for unused address sets
        print("\nAddress Sets")
        total,assigned,unassigned = find_unused(config,address_groups,applied_addresses)
        if len(total) != 0:
            print("\tTotal:")
            for i in total:
                print("\t - " + i)
        if len(assigned) != 0:
            print("\tAssigned:")
            for i in assigned:
                print("\t - " + i)
        if len(unassigned) != 0:
            print("\tUnassigned:")
            for i in unassigned:
                print("\t - " + i)
        # report_unused(total,assigned,unassigned)
        # Remediate unused inet filters to a device specific file
        # command_string = "test "
        # filename = cleanup_dir + hostname + "-addresses.txt"
        # delete_cleanup(unassigned,command_string,filename)

    # Stop the clock
    print('\n\nTotal execution time:', time.time()-test_start,'seconds\n')
    print('Call it', round(time.time()-test_start,3),'seconds...\n')

